const functions = require('firebase-functions');
const google = require('googleapis');
const admin = require('firebase-admin');
const cors = require('cors')({origin: true});
const config = functions.config();
const app = admin.initializeApp(config.firebase);


exports.getAccessToken = functions.https.onRequest((req, resp) => {
    // Enable CORS using 'cors' express middleware
    cors(req, resp, () => {
        const googleJWTClient = new google.auth.JWT(
                "testing-service-account@adroit-cortex-133423.iam.gserviceaccount.com",
                null,
                "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDCpccDuwKzWq3v\nsx9mMcqVHWXq6BCJ7siWjtH0Cx9l+YNCmDcT15+f8tkhsiWs2k6xh1N9TKLViarj\nNOH18rjSBCYBdMRiwMRB6+1K0DhhucjO14nkUFaF0cYarCDIz7tQi2i3SjBUGY76\nmGM+N7Rb//kVU0fgnp9eCERiFc+AV/c9cdLav+PVL+tJymynMDNzNqeWjAa7NgM2\n7vQT91y9SLe9Qnqy5KWnUYMcn5ex8IINQIy0sW8GD+Yn/ejvXjTz9s643xYBZqVT\npMsOHzXpUkZJDmHfGFXXEKIExitL8GIjRaTsQhZsS9DyoAeIYwsl1Uhj6F17H9C+\nUMrPFKXlAgMBAAECggEAKXytEPH8rt6ugQ8UK7OEIsk7zlg9IjEXZuO1A0eVnyYR\n4JFR9/Q27SAVQJXYauGy8SrPVD8P2+S2XCwK12GvG/BPa3MIYqYEjqprDs2Xnti6\n4L9XACdRcb9W8ptqd3KzUYUNQjfZWiuCao4tZPb5hbxmyjFQeiNFA4VOkftlGU0G\nts8+cO9YLpsT2JHedEJbA3TBBmWyrTGL/ZGiptKXHskHOsH7huAY0HiTAZewMGO9\ndExjkLT0bUD+A2JaD2Ejnv3NkDuQTJmv5ZpzMsqgPjmvNWSPoKAiL/OewNXSfJpo\nMdxuTv2P/N15QnApAwxk6gqUuWj/PCo6zcHeeCUnsQKBgQD6emoMR7V8SJTVC+c2\n0b2lTV8Zoducmp0EzZuaZyi4TKTxCySy66DAsd6FfS3FYn8QhklLVNm/kzIRD6OA\ngFJ4aOg03onDK7JQ1IWGHlFl1pktaRMF+dFWuC1fW/ssAdFwAhaQ7Co66kol4ghW\nfMmzvGdv/DGr9sW0b/psV3tyaQKBgQDG8EfHKFX96eKeK4d3E0uWUODBRxU/go9s\neyC+2+Ai71TLv679Hx9RyHCzRHBG4vn0YYSA8I0/fEwXczrU5chSIBfa51ot/EYT\nP099uj8lYO9DgIMIcxh8d11Xc7wsika8wbK8Xxo7L1yhupXsh+AxsjbzhR/VHTe9\nK8m0nN0wHQKBgQDryGgjfy07/Sb9FeNlGzMmdBrdihus426Dovii9BDEzIFH1nGJ\na307ZzRM/HUlpX5vE7nTC+CeEfXDUbFbuW/ooPhVLFcKPgvxQt0eT1PYRZaTxKF/\nspZ4x48OgQuDdYKtqYmMioOGSh3aY1ft8jH825GTPReVBQs+u1pEumUeYQKBgE+q\nANRb7rOTaVB1lfW1IFu9X4OGd5fLLKwAUoibfBIBTS+aPvTV9bDGV3syONPqKLYY\nJvuLWMTHfaEWX8TJj4Jzn5nlc1Ne5qDmhWqPnqHVQ3sAh3hmvrN5CUPv8LbVWKZV\n2sFcRCDXQnzxPwBNGRZNi3vRH4I0BRIX6toMxvZxAoGBAJODe9u3qzGMKm/KfyyA\nOXQDRIPM1KOs3zvjbL2jv4QyHyant+hmRku4ABwprsxOMbl8lcN03GB+Dy8m/RTO\nprqLSaMRchDH3x+Pp6V7E7NLl6I3vtQgwjkNQbakqKypeVzNm9rm86nYdvmEfoUA\nXm/CjN347ORk0Nj8bPujUeXn\n-----END PRIVATE KEY-----\n",
                ['https://www.googleapis.com/auth/spreadsheets'],
                'admin@programmedarwin.com'
            );

            googleJWTClient.authorize((error, accessToken) => {
                if(error) {
                    return console.error("Couldn't get access token", e);
                } else {
                    return saveToken(resp, accessToken);
                    // resp.send(accessToken)
                    // return console.log(accessToken);
                }
            });

        });
});

exports.getSavedToken = functions.https.onRequest((req, resp) => {

    // Enable CORS using the 'cors' express middleware
    cors(req, resp, () => {
        const tokenRef = app.database().ref('/token');
        tokenRef.once('value')
            .then(function(snapshot) {
                var token = snapshot.val();
                resp.send(token);
            });
    });
});

function saveToken(resp, tokenObj) {
    const tokenRef = app.database().ref('/token');
    tokenRef.set(tokenObj)
        .then(function() {
            return resp.send(
                {
                    access_token: tokenObj.access_token,
                    token_type: tokenObj.token_type,
                    expiry_date: tokenObj.expiry_date,
                    refresh_token: tokenObj.refresh_token,
                    tokenSaved: true,
                    error: ""
                }
            );
        })
        .catch(function(error) {
            return resp.send({token: tokenObj, tokenSaved: false, error: e});
        });
}

exports.testSaveToken = functions.https.onRequest((req, resp) => {
    const tokenRef = app.database().ref('/token');
    tokenRef.set(
        {
            "access_token": "ya29.Gn5lBH6E3XeDM1wkccgGhUyzuojw7Qv_W3OEMSjBz_bgJycpYYqZQefyz98H4QHkgJaGytlo3ovBXKRQoj1h-lYBijY0Ywxw3LkRzl2HguB-hHENZeup_Loro-Pd6lJDW7hXg0bydAkrBLeaxylilwb1pFG4lCgWX1zICJ34X6E",
            "token_type": "Bearer",
            "expiry_date": 1497098348000,
            "refresh_token": "jwt-placeholder"
        }
    )
        .then(function() {
            return resp.send({tokenSaved: true, error: ""});
        })
        .catch(function(error) {
            return resp.send({tokenSaved: false, error: e});
        });
});
