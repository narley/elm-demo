const TimelineFactory = function(objConfig) {
    let dataTableFiltered = [],
        dataCounterFiltered,
        view,
        containerElement,
        timeline,
        timelineOptions,
        data,
        dataCounter,
        dateFormatter,
        convertColumns,
        applyDiamonds;


    let defaults = {
        dataSource: undefined,
        containerDivId: "chart-1"
    };

    let settings = Object.assign({}, defaults, objConfig);

    containerElement = document.getElementById(settings.containerDivId);

    // Get data for table element
    data = new google.visualization.arrayToDataTable(settings.dataSource);

    timeline = new google.visualization['Timeline'](containerElement);
    timelineOptions = {
        timeline: {
            groupByRowLabel: settings.groupByRowLabel,
            rowLabelStyle: {
                fontSize: settings.rowLabelFontSize
            },
            barLabelStyle: {
                fontSize: settings.barLabelFontSize
            },
            showBarLabels: settings.showBarLabels
        },
        enableInteractivity: settings.enableInteractivity
    };

    dateFormatter = new google.visualization.DateFormat({pattern: 'dd/MM/yyyy'});
    function changeColDataType (colIndex, dataType) {
        data.insertColumn(colIndex, dataType, data.getColumnLabel(colIndex));
        // copy values from column old column (colIndex + 1) to new column (colIndex), converted to data type
        for (var i = 0; i < data.getNumberOfRows(); i++) {
            var val = data.getValue(i, colIndex + 1);

            if (val && val != '' && val != null) {

                if(dataType === 'number') {
                    data.setValue(i, colIndex, new Number(val).valueOf());
                }
                if(dataType === 'date') {
                    let valToArr = val.split('/');
                    let [day, month, year] = [...valToArr];
                    data.setValue(i, colIndex, new Date(parseInt(year), parseInt(month) - 1, parseInt(day)));
                }
                if(dataType === 'string') {
                    data.setValue(i, colIndex, val.toString());
                }
            }
        }
        // remove the old column (colIndex + 1)
        data.removeColumn(colIndex + 1);
        return this;
    }

    function applyNumberFormat(options, sourceColumns) {
        let formatter = new google.visualization.NumberFormat(options);
        formatter.format(data, sourceColumns);
        return this;
    }

    function drawTimeline() {
        timeline.draw(data, timelineOptions);
        return this;
    }


    /* Apply SVG Diamond */
    applyDiamonds = function() {
        var barAreas = $('rect');
        var milestones = [].filter.call(barAreas, function(item) {
            return item.getAttribute('width') <= 3;
        });

        milestones.forEach(function(milestone, idx) {
            var x = milestone.getBBox().x;
            var y = milestone.getBBox().y;
            var milestoneLabel = milestone.nextSibling;
            var milestoneLabelX = milestoneLabel.getBBox().x;
            var diamondIcon;

            milestone.setAttribute('class', 'bar-area');
            milestoneLabel.setAttribute('class', 'bar-label');

            if(milestoneLabelX < x) {
                milestoneLabel.setAttribute('x', milestoneLabelX - 5);
                milestoneLabel.setAttribute('text-anchor', 'start');
            } else {
                milestoneLabel.setAttribute('x', milestoneLabelX + 5);
            }

            if(milestoneLabel.textContent.trim() === 'Job 1') {
                milestoneLabel.setAttribute('text-anchor', 'middle');
            }

            milestoneLabel.innerHTML = milestoneLabel.innerHTML;

            diamondIcon = createDiamond({
                x: x,
                y: y + 2,
                width: 13,
                height: 13
            });

            $(milestone).parent().append(diamondIcon);
        });

        function createDiamond(options) {
            var cx = options.x + options.width/2;
            var cy = options.y + options.height/2;
            var rect = document.createElementNS("http://www.w3.org/2000/svg","rect");
            rect.setAttributeNS(null, 'class', 'diamond');
            rect.setAttributeNS(null, 'x', options.x);
            rect.setAttributeNS(null, 'y', options.y);
            rect.setAttributeNS(null, 'height', options.height);
            rect.setAttributeNS(null, 'width', options.width);
            rect.setAttributeNS(null, 'fill', '#000000');
            rect.setAttributeNS(null, 'transform', 'translate(-5) rotate(45 ' + cx + ' ' + cy + ')');
            return rect;
        }

        return this;
    }

    return {
        drawTimeline,
        changeColDataType,
        applyNumberFormat,
        applyDiamonds
    };
};


module.exports = TimelineFactory;
