port module Ports exposing (..)


import Model exposing (..)
import ChartsConfig exposing (..)

-- Outbound


port saveTokenToLocalStorage : String -> Cmd msg


port getTokenFromLocalStorage : String -> Cmd msg


port sendToChart : TimelineConfig -> Cmd msg


port saveElement : Element -> Cmd msg


port updateElementState : Element -> Cmd msg



-- Inbound


port authUser : (User -> msg) -> Sub msg


port loadSpreadsheetData : (String -> msg) -> Sub msg


port loadSheetsData : (String -> msg) -> Sub msg


port sendTokenFromLocalStorage : (AccessToken -> msg) -> Sub msg


port elementState : (Element -> msg) -> Sub msg


port updateElementContent : (String -> msg) -> Sub msg
