module App exposing (..)

import Debug exposing (..)
import Helpers exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Html.Events.Extra exposing (..)
import Http exposing (..)
import Json.Decode as Json exposing (Decoder, int, list, string)
import Json.Decode.Pipeline as PL exposing (decode, required, requiredAt)
import List.Extra exposing (..)
import Messages as Msg exposing (..)
import Model exposing (..)
import Ports exposing (..)
import Requests exposing (..)
import Update exposing (..)
import View exposing (..)
import Bootstrap.Navbar as Navbar
import Navigation exposing (Location)



---- PROGRAM ----


main : Program Flags Model Msg
main =
    Navigation.programWithFlags
        UrlChange
        { view = view
        , init = init
        , update = update
        , subscriptions = subscriptions
        }


init : Flags -> Location -> ( Model, Cmd Msg )
init flags location =
-- The navbar needs to know the initial window size, so the inital state for a navbar requires a command to be run by the Elm runtime
    let
        (navbarState, navbarCmd) =
            Navbar.initialState NavbarMsg

        page =
            Helpers.locationToPage location
    in
      ( { message = "Your Elm App is working!"
        , navbarState = navbarState
        , logo = flags.path
        , token = ""
        , sheets = []
        , sheetsData = []
        , error = ""
        , selectedProject = ""
        , elements = initialElements
        , elementOnFocus = False
        , elementIsLocked = False
        , showSaveBtn = False
        , user = emptyUser
        , page = page
        }
      , Cmd.batch [getSavedAccessToken, navbarCmd]
        -- , getSheets flags.token
      )

initialElements : List (String, Element)
initialElements =
    [ ( "pmCommentary", pmCommentary )
    , ( "scheduleCommentary", scheduleCommentary )
    ]

---- UPDATE ----
---- VIEW ----
-- view : Model -> Html Msg
-- view model =
--     div ([])
--         [ img [ src model.logo ] []
--         , div [] [ text model.message ]
--         , div []
--             [ projectList "Project List" model.sheetsData
--             ]
--         , div [ id "timeline" ] []
--         ]
-- projectList : String -> List SheetData -> Html Msg
-- projectList sheetName sheetsData =
--     let
--         projectListSheet =
--             sheetsData
--                 |> List.Extra.find (\item -> item.sheetName == sheetName)
--                 |> Maybe.withDefault { data = [], sheetName = "" }
--         projectList =
--             projectListSheet.data
--                 |> List.concat
--                 |> List.tail
--                 |> Maybe.withDefault []
--         options =
--             (List.map (\project -> option [ value project ] [ text project ]) projectList)
--     in
--         Html.select [ on "change" (Json.map SelectedProject targetValueMaybe) ]
--             ((option [ value "Select Project" ] [ text "Select Project" ])
--                 :: (option [ value "All Projects" ] [ text "All Projects" ])
--                 :: options
--             )
-- Subscriptions


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ --loadSpreadsheetData (\accessToken -> LoadData accessToken)
          --, loadSheetsData (\accessToken -> LoadSheets accessToken)
          sendTokenFromLocalStorage (\accessToken -> GetTokenFromLocalStorage accessToken)
        , elementState (\element -> ElementState element)
        , authUser (\user -> AuthUser user)
        , Navbar.subscriptions model.navbarState NavbarMsg
        ]
