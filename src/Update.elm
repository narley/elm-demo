module Update exposing (..)

import ChartsConfig exposing (..)
import Helpers exposing (..)
import Http exposing (..)
import List.Extra as LX exposing (..)
import Maybe exposing (..)
import Maybe.Extra as MX exposing (..)
import Messages as Msg exposing (..)
import Model exposing (..)
import Ports exposing (..)
import Requests exposing (..)
import Dict exposing (..)
import Routes exposing (..)
import Navigation exposing (..)
import Debug exposing (..)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        UrlChange location ->
            let
                _ =  Debug.log "Loc:" location

                page = Helpers.locationToPage location
            in
            { model | page = page } ! []

        GetToken (Ok data) ->
            { model | token = data.token } ! [ getSpreadsheetData data.token (defineRangeAttribute sheetList) ]

        GetToken (Err error) ->
            { model | error = toString error } ! []

        -- GetTokenFromLocalStorage (Just token) ->
        --     { model | token = token } ! []
        -- GetTokenFromLocalStorage Nothing ->
        -- model ! []
        GetTokenFromLocalStorage data ->
            { model | token = data.token } ! [ getSheets data.token ]

        LoadData token ->
            model ! []

        LoadSheets token ->
            { model | token = token } ! [ getSheets token ]

        GetData (Ok data) ->
            let
                sheetsData =
                    List.map
                        (\valueRange ->
                            SheetData (convertSheetName valueRange.range) valueRange.values
                        )
                        data.valueRanges
            in
                { model
                    | sheetsData = sheetsData
                    , error = ""
                }
                    -- ! [ sendToChart sheetsData ]
                    ! []

        GetData (Err error) ->
            -- model ! []
            let
                ( err, cmd ) =
                    if httpErrorString error == "401" then
                        ( "Not authorized", [ getNewAccessToken ] )
                    else
                        ( "Problem with Ranges", [] )
            in
                { model | error = err } ! cmd

        GetSheets (Ok data) ->
            let
                head =
                    (Maybe.withDefault "" <|
                        Maybe.map (\x -> "ranges=" ++ x.title) <|
                            List.head data.sheets
                    )
                        :: []

                tail =
                    List.map (\x -> "&ranges=" ++ x.title) <|
                        Maybe.withDefault [] <|
                            List.tail data.sheets

                test =
                    List.length (List.concat [ head, tail ])

                sheetsRange =
                    String.join "" (List.take 40 (List.concat [ head, tail ]))

                sheets =
                    List.map (\sheet -> sheet.title) data.sheets
            in
                { model | sheets = sheets } ! [ getSpreadsheetData model.token sheetsRange ]

        GetSheets (Err error) ->
            let
                e =
                    Debug.log "Erro: " httpErrorString error
            in
                model ! [ getNewAccessToken ]

        SelectedProject (Just project) ->
            let
                sheetData =
                    model.sheetsData
                        |> LX.find (\item -> item.sheetName == "All Projects")

                timelineData =
                    case sheetData of
                        Just sheet ->
                            sheet.data

                        Nothing ->
                            [ [] ]

                -- timelineHeader =
                --     Maybe.withDefault [] (List.head timelineData)
                filteredData =
                    if project == "All Projects" then
                        timelineData
                            |> LX.filterNot (\arr -> findProject "Temporary Village" "Project" arr)
                            |> LX.filterNot (\arr -> findProject "Conveyor Bridge" "Project" arr)
                            |> LX.filterNot (\arr -> findProject "Permitting" "Project" arr)
                            |> List.map (\arr -> List.take 4 arr)
                            |> LX.unique
                    else
                        timelineData
                            |> List.filter (\arr -> findProject project "Project" arr)
                            |> List.map (\arr -> List.take 4 arr)

            in
                if project /= "Select Project" then
                    { model | selectedProject = project } ! [ sendToChart (timelineConfig filteredData "timeline") ]
                else
                    model ! []

        SelectedProject Nothing ->
            model ! []

        HandleInput element content ->
            let
                dict =
                    Dict.fromList model.elements

                newElement =
                    Dict.update element.id (\elm -> Just { element
                                                             | content = content
                                                             , lastEditor = model.user
                                                         }) dict
            in
                { model | elements = Dict.toList newElement } ! []

        ElementOnFocus element ->
            let
                dict =
                    Dict.fromList model.elements

                newElement =
                    Dict.update element.id
                        (\elm -> Just { element
                                          | isLocked = True
                                          , currentEditor = model.user
                                      }) dict
            in
                { model
                    | elements = Dict.toList newElement
                    , showSaveBtn = True
                } ! [ updateElementState (Maybe.withDefault emptyElement (Dict.get element.id newElement)) ]

        ElementOnBlur element ->
            let
                dict =
                    Dict.fromList model.elements

                newElement =
                    Dict.update element.id
                        (\elm -> Just { element
                                          | isLocked = False
                                          , currentEditor = emptyUser
                                      }) dict
            in
                { model
                    | elements = Dict.toList newElement
                    , showSaveBtn = False
                } ! [ updateElementState (Maybe.withDefault emptyElement (Dict.get element.id newElement)) ]

        SaveContent element ->
            model ! [ saveElement element ]

        -- TODO: save to spreadsheet
        ElementState element ->
            let
                dict =
                    Dict.fromList model.elements

                newElement =
                    Dict.update element.id (\_ -> Just element) dict
            in
              { model | elements = Dict.toList newElement } ! []

        AuthUser user ->
            { model | user = user } ! []


        NavbarMsg state ->
            { model | navbarState = state } ! []


        NavigateTo page ->
            let
                cmd =
                    Navigation.newUrl (Routes.pageToPath page)
            in
                model ! [ cmd ]
