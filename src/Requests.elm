module Requests exposing (..)


import Decoders exposing (..)
import Model exposing (..)
import Http exposing (..)
import Messages as Msg exposing (..)


ssId : String
ssId =
    "1XFlnHP3tDO-z9DlV0PtwFcSZNtaGcEYpUUWmpa5aUIQ"


baseUrl : String
baseUrl =
    "https://sheets.googleapis.com/v4/spreadsheets/" ++ ssId


requestSavedAccessToken : Request AccessToken
requestSavedAccessToken =
    Http.get "https://us-central1-mace-darwin-ame-timeline.cloudfunctions.net/getSavedToken" accessTokenDecoder


requestNewAccessToken : Request AccessToken
requestNewAccessToken =
    Http.get "https://us-central1-mace-darwin-ame-timeline.cloudfunctions.net/getAccessToken" accessTokenDecoder


requestSSData : String -> String -> Request SSData
requestSSData accessToken ranges =
    let
        url : String -> String
        url baseUrl =
            baseUrl
                ++ "/values:batchGet?"
                ++ ranges
                ++ "&access_token="
                ++ accessToken
    in
        Http.get (url baseUrl) spreadsheetDecoder


requestSheets : String -> Request Sheets
requestSheets accessToken =
    let
        url : String -> String
        url baseUrl =
            baseUrl
                ++ "?access_token="
                ++ accessToken
    in
        Http.get (url baseUrl) sheetsDecoder


getSpreadsheetData : String -> String -> Cmd Msg
getSpreadsheetData accessToken ranges =
    Http.send GetData <| requestSSData accessToken ranges


getSheets : String -> Cmd Msg
getSheets accessToken =
    Http.send GetSheets <| requestSheets accessToken


getSavedAccessToken : Cmd Msg
getSavedAccessToken =
    Http.send GetToken <| requestSavedAccessToken


getNewAccessToken : Cmd Msg
getNewAccessToken =
    Http.send GetToken <| requestNewAccessToken
