module Helpers exposing (..)


import Http exposing (..)
import List.Extra as LX exposing (..)
import Model exposing (..)
import Navigation exposing (..)
import Routes exposing (..)
import Debug exposing (..)


findProject : String -> String -> List String -> Bool
findProject project projectColName list =
    let
        found =
            (LX.last list) == (Just project) || (LX.last list) == (Just projectColName)
    in
        found


findSheet : String -> String -> Bool
findSheet sheetName range =
    if (String.split "'" range |> List.tail |> Maybe.withDefault [] |> List.head |> Maybe.withDefault "") == sheetName then
        True
    else
        False


convertSheetName : String -> String
convertSheetName str =
    str
        |> String.split "!"
        |> List.head
        |> Maybe.withDefault ""
        |> String.split "'"
        |> LX.find (\x -> x /= "")
        |> Maybe.withDefault ""


defineRangeAttribute : List String -> String
defineRangeAttribute rangeList =
    let
        head =
            (Maybe.withDefault "" <|
                Maybe.map (\x -> "ranges=" ++ x) <|
                    List.head rangeList
            )
                :: []

        tail =
            List.map (\x -> "&ranges=" ++ x) <|
                Maybe.withDefault [] <|
                    List.tail rangeList

        test =
            List.length (List.concat [ head, tail ])

        sheetsRange =
            String.join "" (List.take 40 (List.concat [ head, tail ]))
    in
        sheetsRange


httpErrorString : Error -> String
httpErrorString error =
    case error of
        BadUrl text ->
            "Bad Url: " ++ text

        Timeout ->
            "Http Timeout"

        NetworkError ->
            "Network Error"

        BadStatus response ->
            toString response.status.code

        -- "Bad Http Status: " ++ toString response
        BadPayload message response ->
            "Bad Http Payload: "
                ++ toString message
                ++ " ("
                ++ toString response.status.code
                ++ ")"



locationToPage : Location -> Page
locationToPage location =
    let
        page =
            location
                |> Routes.pathParser
                |> Maybe.withDefault (NotFound location)
    in
        page
