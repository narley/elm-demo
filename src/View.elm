module View exposing (..)

import Bootstrap.Dropdown as Dropdown
import Bootstrap.Form as Form
import Bootstrap.Form.Input as Input
import Bootstrap.Form.Textarea as Textarea
import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Bootstrap.Grid.Row as Row
import Bootstrap.Button as Button
import Bootstrap.Badge as Badge
import Bootstrap.Navbar as Navbar
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Html.Events.Extra exposing (..)
import Html.Lazy exposing (..)
import Json.Decode as Json exposing (Decoder, int, list, string)
import List.Extra as LX exposing (..)
import Maybe.Extra as MX exposing (..)
import Messages exposing (..)
import Model exposing (..)
import Debug exposing (..)
import Dict exposing (..)
import Json.Decode as JD exposing (succeed)
import Routes exposing (..)


view : Model -> Html Msg
view model =
    let
        page =
            case model.page of
                Home ->
                    homeView

                Timeline ->
                    timelineView model

                Commentary ->
                    commentaryView model

                NotFound _->
                    notFoundView model
    in
      div []
          [ lazy navbar model
          , Grid.containerFluid [
                ]
              page
          ]


navbar : Model -> Html Msg
navbar model =
    Navbar.config NavbarMsg
        |> Navbar.withAnimation
        |> Navbar.primary
        |> Navbar.brand
           [ href "#" ]
           [ img
                 [ src model.logo
                 , class "d-inline-block align-top"
                 , style [ ( "width", "30px" ) ]
                 , href "#"
                 , onLinkClick (NavigateTo Home)
                 ]
                 []
           -- , text "Elmace"
           ]
        |> Navbar.items
           [ Navbar.itemLink [href "#", onLinkClick (NavigateTo Home) ] [ text "Home" ]
           , Navbar.itemLink [href "#", onLinkClick (NavigateTo Timeline)] [ text "Timeline" ]
           , Navbar.itemLink [href "#", onLinkClick (NavigateTo Commentary) ] [ text "Commentary" ]
           ]
        |> Navbar.view model.navbarState


notFoundView : Model -> List (Html Msg)
notFoundView model =
    let
        page =
            Routes.pageToPath model.page
    in
        [ pageTitle "Ops!"
        , h3 []
            [ text ("Page ")
            , (b [] [ text page ])
            , text " couldn't be found!"
            ]
        ]


homeView : List (Html Msg)
homeView =
    [ pageTitle "Elm Demo"
    , p []
      [ text "This application takes advantage of the following stack:"]
    , ul []
      [ li [ class "item" ] [ text "Elm" , p [] [ i [] [ text "Functional programming language that complies down to Javascript." ] ] ]
      , li [ class "item" ] [ text "Elm Ports", p [] [ i [] [ text "A way to interop with Javascript. Think of if as server(Elm)-client(JS) architecture." ] ] ]
      , li [ class "item" ] [ text "Elm Navigation", p [] [ i [] [ text "A package (together with UrlParser) to create Single Page Applications." ] ] ]
      , li [ class "item" ] [ text "Elm Bootstrap", p [] [ i [] [ text "A package that aims to make it pleasant and reasonably type safe to use the upcoming Twitter Bootstrap 4 CSS Framework in Elm applications." ] ] ]
      , li [ class "item" ] [ text "Elm Format", p [] [ i [] [ text "A package that aims to make it pleasant and reasonably type safe to use the upcoming Twitter Bootstrap 4 CSS Framework in Elm applications." ] ] ]
      , li [ class "item" ] [ text "Javascript", p [] [ i [] [ text "Our beloved programming language."] ] ]
      , li [ class "item" ] [ text "Firebase Storage", p [] [ i [] [ text "Fast and secure static hosting for web apps." ] ] ]
      , li [ class "item" ] [ text "Firebase Functions", p [] [ i [] [ text "Automatically run backend code in response to events triggered by Firebase features and HTTPS requests." ] ] ]
      , li [ class "item" ] [ text "Firebase Database", p [] [ i [] [ text "he Firebase Realtime Database is a cloud-hosted database. Data is stored as JSON and synchronized in realtime to every connected client." ] ] ]
      , li [ class "item" ] [ text "Google Sheets API", p [] [ i [] [ text "Gives full control over the content and appearence of spreadsheet data." ] ] ]
      , li [ class "item" ] [ text "Google Visualisation API", p [] [ i [] [ text "Interactive charts for browsers and mobile devices." ] ] ]
      , li [ class "item" ] [ text "Google Service Account", p [] [ i [] [ text "An account that belongs to your application instead of to an individual end user. Your application calls Google APIs on behalf of the service account, so users aren't directly involved. " ] ] ]
      ]
    ]

timelineView : Model -> List (Html Msg)
timelineView model =
    [ pageTitle "Darwin AME Timeline"
    , projectListDropdown "Project List" model.sheetsData
    , timeLineChartContainer
    ]


commentaryView : Model -> List (Html Msg)
commentaryView model =
    [ pageTitle "Commentary"
    , pmCommentaryView model
    , scheduleCommentaryView model
    ]


pageTitle : String -> Html msg
pageTitle title =
      Grid.row [ Row.attrs [ class "title-wrapper" ] ]
          [ Grid.col [ Col.xs12 ]
              [ h3 [ class "app-title" ]
                  [ text title]
              ]
          ]


projectListDropdown : String -> List SheetData -> Html Msg
projectListDropdown sheetName sheetsData =
    let
        projectListSheet =
            sheetsData
                |> LX.find (\item -> item.sheetName == sheetName)
                |> Maybe.withDefault { data = [], sheetName = "" }

        projectList =
            projectListSheet.data
                |> List.concat
                |> List.tail
                |> Maybe.withDefault []

        options =
            List.map (\project -> option [ value project ] [ text project ]) projectList
    in
        Grid.row []
            [ Grid.col [ Col.xs8, Col.lg2 ]
                [ Html.select
                    [ on "change" (Json.map SelectedProject targetValueMaybe)
                    , class "selectpicker form-control"
                    , id "select-timeline"
                    ]
                    (option [ value "Select Project" ] [ text "Select Project" ]
                        :: option [ value "All Projects" ] [ text "All Projects" ]
                        :: options
                    )
                ]
            ]


timeLineChartContainer : Html Msg
timeLineChartContainer =
    Grid.row []
        [ Grid.col [ Col.xs12 ]
            [ div [ id "cover-empty" ] []
            , div [ id "timeline" ] []
            ]
        ]


pmCommentaryView : Model -> Html Msg
pmCommentaryView model =
    let
        dict =
            Dict.fromList model.elements
                    |> Dict.get "pmCommentary"
        element =
            Maybe.withDefault emptyElement dict
    in
    Grid.row []
        [ Grid.col [ Col.xs8, Col.lg4 ]
            [ div [ id "pm-commentary-group" ]
                [ Form.group []
                    [ label [ for "pm-commentary", id "pm-commentary-label" ] [ text "PM Commentary:" ]
                    , Textarea.textarea
                        [ Textarea.id "pm-commentary"
                        , Textarea.rows 4
                        , Textarea.onInput (HandleInput element)
                        , Textarea.value element.content
                        , Textarea.attrs [ onFocus (ElementOnFocus element) ]
                        , Textarea.attrs [ onBlur (ElementOnBlur element) ]
                        , Textarea.attrs [ disabled (element.isLocked && element.currentEditor.uid /= model.user.uid) ]
                        ]
                    -- , Button.button
                    --     [ Button.primary
                    --     , Button.small
                    --     , Button.attrs
                    --         [ id "pm-commentary-save-btn"
                    --         , classList [ ( "invisible", element.editor.uid /= model.user.uid ) ]
                    --         , onClick (SaveContent element)
                    --         ]
                    --     ]
                    --     [ text "Save" ]
                    , Badge.badgeInfo
                        [ classList
                              [ ( "invisible", not (element.isLocked && element.currentEditor.uid /= model.user.uid))
                              ]
                        , id "pm-commentary-badge"
                        ]
                        [ text (element.currentEditor.displayName ++ " is editing...") ]
                    ]
                ]
            ]
        ]


scheduleCommentaryView : Model -> Html Msg
scheduleCommentaryView model =
    let
        dict =
            Dict.fromList model.elements
                    |> Dict.get "scheduleCommentary"
        element =
            Maybe.withDefault emptyElement dict
    in
    Grid.row []
        [ Grid.col [ Col.xs8, Col.lg4 ]
            [ div [ id "schedule-commentary-group" ]
                [ Form.group []
                    [ label [ for "schedule-commentary", id "schedule-commentary-label" ] [ text "Schedule Commentary:" ]
                    , Textarea.textarea
                        [ Textarea.id "schedule-commentary"
                        , Textarea.rows 4
                        , Textarea.onInput (HandleInput element)
                        , Textarea.value element.content
                        , Textarea.attrs [ onFocus (ElementOnFocus element) ]
                        , Textarea.attrs [ onBlur (ElementOnBlur element) ]
                        , Textarea.attrs [ disabled (element.isLocked && element.currentEditor.uid /= model.user.uid) ]
                        ]
                    -- , Button.button
                    --     [ Button.primary
                    --     , Button.small
                    --     , Button.attrs
                    --         [ id "schedule-commentary-save-btn"
                    --         , classList [ ( "invisible", element.currentEditor.uid /= model.user.uid ) ]
                    --         , onClick (SaveContent model.comment1)
                    --         ]
                    --     ]
                    --     [ text "Save" ]
                    , Badge.badgeInfo
                        [ classList
                              [ ( "invisible", not (element.isLocked && element.currentEditor.uid /= model.user.uid))
                              ]
                        , id "schedule-commentary-badge"
                        ]
                        [ text (element.currentEditor.displayName ++ " is editing...") ]
                    ]
                ]
            ]
        ]



onLinkClick : msg -> Attribute msg
onLinkClick msg =
    onWithOptions "click"
        { stopPropagation = True
        , preventDefault = True
        }
        (JD.succeed msg)
