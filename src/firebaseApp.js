/* Firebase Settings */
const firebase = require('firebase/app');
require('firebase/database');
require('firebase/auth');

const firebaseApp = function(elm) {
    const provider = new firebase.auth.GoogleAuthProvider();
    // provider.addScope('https://www.googleapis.com/auth/spreadsheets.readonly');

    /* Initialize Firebase */
    var config = {
        apiKey: "AIzaSyCbwCZwJUVKA0ybnln8q_nAWYI5lO47doo",
        authDomain: "mace-darwin-ame-timeline.firebaseapp.com",
        databaseURL: "https://mace-darwin-ame-timeline.firebaseio.com",
        projectId: "mace-darwin-ame-timeline",
        storageBucket: "mace-darwin-ame-timeline.appspot.com",
        messagingSenderId: "907463968071"
    };
    firebase.initializeApp(config);


    /* Authenticate user */
    function authenticateUser() {
        // Listens for sign-in state changes
        firebase.auth().onIdTokenChanged(function(user) { updateSigninStatus(user); });

        /*
        * Check if user is signed-in and if it's authorized to access the app.
        */
        function updateSigninStatus(user) {
            if(user) {
                // TODO: Send user to Elm
                elm.ports.authUser.send(
                    {
                        uid: user.uid,
                        apiKey: user.m,
                        email: user.email,
                        displayName: user.displayName,
                        photoUrl: user.photoURL
                    }
                );


                // Firebase token
                // var userToken;
                // user.getIdToken().then(function(token) { userToken = token; } );

                console.log("Logged in user: ");
                console.log(user.toJSON());
            } else {
                console.log('Authenticating user...');
                authenticate();
            }
        }

        function authenticate() {
            firebase.auth().signInWithRedirect(provider);
            firebase.auth().getRedirectResult().then(function(result) {
                // This gives you a Google Access Token. You can use it to access the Google API.
                // Don't need this token.
                // Access token comes from Service Account JWT setup.
                // var token = result.credential.accessToken;

                // The signed-in user info.
                // var user = result.user;

            }).catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                // The email of the user's account used.
                var email = error.email;
                // The firebase.auth.AuthCredential type that was used.
                var credential = error.credential;

                // TODO: Send error to Elm
                // elm.ports.authError.send(error);

            });
        }
    }

    return {
        database: firebase.database,
        authenticateUser
    };
};

module.exports = firebaseApp;
