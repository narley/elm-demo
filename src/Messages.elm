module Messages exposing (..)


import Http exposing (..)
import Model exposing (..)
import Bootstrap.Navbar as Navbar
import Navigation exposing (Location)
import Routes exposing (..)


type Msg
    = NoOp
    | GetToken (Result Http.Error AccessToken)
    | GetTokenFromLocalStorage AccessToken
    | LoadData String
    | LoadSheets String
    | GetData (Result Http.Error SSData)
    | GetSheets (Result Http.Error Sheets)
    | SelectedProject (Maybe String)
    | HandleInput Element String
    | ElementOnFocus Element
    | ElementOnBlur Element
    | ElementState Element
    | SaveContent Element
    | AuthUser User
    | NavbarMsg Navbar.State
    | UrlChange Location
    | NavigateTo Page
