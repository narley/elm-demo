import './main.css';
const logoPath = require('./logo.svg');
const Elm = require('./App.elm');
const timeline = require('./timeline-factory');
const root = document.getElementById('root');
// const token = localStorage.getItem('token');
const token = "";
const elm = Elm.App.embed(root, {path: logoPath, token: token });
const fb = require('./firebaseApp')(elm);

// if(token === null) {
//     token = "";
// } else {
//     token = token;
// }

google.charts.load('current', { 'packages': [ 'timeline' ] } );
google.charts.setOnLoadCallback(init);


function init() {
    fb.authenticateUser();
    var renderTimeline = function(timelineConfig) {
        let tl = timeline(timelineConfig);
        tl
            .changeColDataType(2, "date")
            .changeColDataType(3, "date")
            .drawTimeline()
            .applyDiamonds();
    };


    elm.ports.sendToChart.subscribe(
        function(timelineConfig) {
            renderTimeline(timelineConfig);
        }
    );

    elm.ports.updateElementState.subscribe(
        function(element) {
            fb.database().ref('elements/' + element.id).transaction(function(currentData) {
                return element;
            });
        }
    );

    elm.ports.saveElement.subscribe(
        function(element) {
            fb.database().ref('elements/' + element.id ).transaction(function(currentData) {
                return element;
            });
        }
    );


    var pmCommentary = fb.database().ref('elements/pmCommentary/');
    pmCommentary.on('value', function(snapshot) {
        if(snapshot.val() !== null) {
            elm.ports.elementState.send(snapshot.val());
        }
    });

    var scheduleCommentary = fb.database().ref('elements/scheduleCommentary/');
    scheduleCommentary.on('value', function(snapshot) {
        if(snapshot.val() !== null) {
            elm.ports.elementState.send(snapshot.val());
        }
    });







    // elm.ports.saveTokenToLocalStorage.subscribe(
    //     function(token) {
    //         localStorage.setItem('token', token);
    //     }
    // )


    // elm.ports.getTokenFromLocalStorage.subscribe(
    //     function() {
    //         console.log("aaaaaaa");
    //         let token = localStorage.getItem('token');
    //         console.log(token);
    //         elm.ports.sendTokenFromLocalStorage.send(token);
    //     }
    // );

    // $('.selectpicker').selectpicker();
    // $('#select-timeline').on('changed.bs.select', function(e) {
    //     var selectedProject = e.target.value;
    //     console.log('here');
    // });
}
