module Routes exposing (..)


import UrlParser exposing (..)
import Navigation exposing (..)
-- import Messages exposing (..)


type Page
    = Home
    | Timeline
    | Commentary
    | NotFound Location


pageParser : Parser (Page -> a) a
pageParser =
    UrlParser.oneOf
        [ map Home (s "")
        , map Timeline (s "timeline")
        , map Commentary (s "commentary")
        ]

pathParser : Navigation.Location -> Maybe Page
pathParser location =
    UrlParser.parsePath pageParser location


pageToPath : Page -> String
pageToPath page =
    case page of
        Home ->
            "/"

        Timeline ->
            "/timeline"

        Commentary ->
            "/commentary"

        NotFound location->
            toString location.href
