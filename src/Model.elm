module Model exposing (..)

import Bootstrap.Navbar as Navbar
import Routes exposing (..)


type alias Model =
    { message : String
    , navbarState : Navbar.State -- You need to keep track of the view state for the navbar in your model
    , logo : String
    , token : String
    , sheetsData : List SheetData
    , sheets : List String
    , error : String
    , selectedProject : String
    , elements : List (String, Element)
    , elementOnFocus : Bool
    , elementIsLocked : Bool
    , showSaveBtn : Bool
    , user : User
    , page : Page
    }


sheetList : List String
sheetList =
    [ "All Projects", "Project List" ]


type alias Flags =
    { path : String
    , token : String
    }


type alias SSData =
    { spreadsheetId : String
    , valueRanges : List Range
    }


type alias Range =
    { range : String
    , majorDimension : String
    , values : List (List String)
    }


type alias Sheets =
    { sheets : List SheetProperty
    }


type alias SheetProperty =
    { title : String
    }


type alias AccessToken =
    { token : String
    }


type alias SheetData =
    { sheetName : String
    , data : List (List String)
    }


type alias SheetData2 =
    ( String, List (List String) )


type alias Element =
    { id : String
    , isLocked : Bool
    , content : String
    , currentEditor : User
    , lastEditor : User
    }


type alias User =
    { uid : String
    , apiKey : String
    , email : String
    , displayName : String
    , photoUrl : String
    }




-- type alias Project =
--     { id : String
--     , projectName : String
--     , elements : List (String, Element)
--     }

emptyUser : User
emptyUser =
    { uid = ""
    , apiKey = ""
    , email = ""
    , displayName = ""
    , photoUrl = ""
    }


emptyElement : Element
emptyElement =
    Element "" False "" emptyUser emptyUser

pmCommentary : Element
pmCommentary =
    Element "pmCommentary" False "" emptyUser emptyUser


scheduleCommentary : Element
scheduleCommentary =
    Element "scheduleCommentary" False "" emptyUser emptyUser
