module ChartsConfig exposing (..)



type alias TimelineConfig =
    { dataSource : List (List String)
    , containerDivId : String
    , groupByRowLabel : Bool
    , rowLabelFontSize : String
    , barLabelFontSize : String
    , showBarLabels : Bool
    , enableInteractivity : Bool
    }

timelineConfig : List (List String) -> String -> TimelineConfig
timelineConfig data containerId =
    { dataSource = data
    , containerDivId = containerId
    , groupByRowLabel = True
    , rowLabelFontSize = ""
    , barLabelFontSize = "9"
    , showBarLabels = True
    , enableInteractivity = True
    }
