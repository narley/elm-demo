module Decoders exposing (..)



import Model exposing (..)
import Json.Decode as Json exposing (Decoder, int, list, string)
import Json.Decode.Pipeline as PL exposing (decode, required, requiredAt)



spreadsheetDecoder : Decoder SSData
spreadsheetDecoder =
    decode SSData
        |> PL.required "spreadsheetId" Json.string
        |> PL.required "valueRanges" (Json.list rangeDecoder)


rangeDecoder : Decoder Range
rangeDecoder =
    decode Range
        |> PL.required "range" Json.string
        |> PL.required "majorDimension" Json.string
        |> PL.required "values" (Json.list (Json.list string))


sheetPropertyDecoder : Decoder SheetProperty
sheetPropertyDecoder =
    decode SheetProperty
        |> PL.requiredAt [ "properties", "title" ] Json.string


sheetsDecoder : Decoder Sheets
sheetsDecoder =
    decode Sheets
        |> PL.required "sheets" (Json.list sheetPropertyDecoder)


accessTokenDecoder : Decoder AccessToken
accessTokenDecoder =
    decode AccessToken
        |> PL.required "access_token" Json.string



-- |> PL.requiredAt [ "token", "access_token" ] Json.string
-- Http request


